﻿using com.mobilio.common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace com.mobilio.data.Infrastructure
{
    public class MobilioBContextFactory : IDesignTimeDbContextFactory<MobilioDBContext>
    {
        public IConfiguration _Configuration;

        public MobilioBContextFactory(IConfiguration configuration)
        {
            _Configuration = configuration;
        }

        public MobilioDBContext CreateDbContext(string[] args)
        {

            // dotnet ef migrations add  ---MigrationName---  --context MobilioDBConnection
            // dotnet ef migrations add InitialCreate --context MainDBContext --output-dir Migrations/MainDBMigrations

            var optionsBuilder = new DbContextOptionsBuilder<MobilioDBContext>();

      string connection = _Configuration.GetConnectionString(Constants.MobilioDBConnection);
          //        connection = "Server=localhost;Port=5432;Database=edgeprotectordb;Username=postgres;Password=.letmein";
            optionsBuilder.UseNpgsql(connection);            
            return new MobilioDBContext(optionsBuilder.Options);
        }
    } 
}
