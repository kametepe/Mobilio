﻿

using com.mobilio.common;
using com.mobilio.entities;
using com.mobilio.utilities.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.mobilio.data.Infrastructure
{
    public static class MobilioDBSeedData
    {
        public static void EnsureSeedData(this MobilioDBContext db)
        {
            Guid superadmin = Guid.Parse(Constants.DemoSuperAdmin);

           
 

            if (!db.Products.Any())
            {
                List<Product> products = new[]
                {
                     new Product
				{
       
                    Name ="Ergonomic Chair",
                    Description ="Ergonomic Chair",
                    Price = 43.00,
                    SmallImage ="/images/product-3.png",
                    BigImage="/images/product-3.png",
                     State = 2,
                     CreationDate = DateTime.UtcNow,
                     EditionDate = DateTime.UtcNow,
                     EditorID =superadmin
				},

				 new Product
				{

					Name ="Kruzo Aero Chair",
					Description ="Kruzo Aero Chair",
					Price = 78.00,
					SmallImage ="/images/product-2.png",
					BigImage="/images/product-2.png",
					 State = 2,
					 CreationDate = DateTime.UtcNow,
					 EditionDate = DateTime.UtcNow,
					 EditorID =superadmin
				},
                  new Product
                {

                    Name ="Nordic Chair",
                    Description ="Kruzo Aero Chair",
                    Price = 50.00,
                    SmallImage ="/images/product-1.png",
                    BigImage="/images/product-1.png",
                     State = 2,
                     CreationDate = DateTime.UtcNow,
                     EditionDate = DateTime.UtcNow,
                     EditorID =superadmin
                }

                }.ToList();

                db.Products.AddRange(products);
                db.SaveChanges();

            }

             
        }
    }
}
