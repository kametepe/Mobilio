﻿
using Microsoft.EntityFrameworkCore;
using System;
using com.mobilio.entities;
 

namespace com.mobilio.data.Infrastructure
{
    public class MobilioDBContext : DbContext
    {
        public MobilioDBContext(DbContextOptions<MobilioDBContext> options)
            : base(options)
        {
            Database.Migrate();

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
             
            
        } 

        public DbSet<Product> Products { get; set; }

        public DbSet<BlogPost> BlogPosts { get; set; }

        public DbSet<Testimony> Testimonies { get; set; } 

    }
}
