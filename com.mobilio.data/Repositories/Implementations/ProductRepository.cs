﻿using com.mobilio.data.Infrastructure;
using com.mobilio.data.Repositories.Interfaces;
using com.mobilio.entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace com.mobilio.data.Repositories.Implementations
{
    public class ProductRepository : IProductRepository
    {
        private MobilioDBContext _context;
        private readonly ILogger<ProductRepository> _logger;
        public ProductRepository(ILogger<ProductRepository> logger, MobilioDBContext context)
        {
            _context = context;
            _logger = logger;
        }
        public List<Product> GetProducts()
        {
            return _context.Products.ToList();
        }
    }
}
