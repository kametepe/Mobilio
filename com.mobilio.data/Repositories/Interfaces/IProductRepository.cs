﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;
using com.mobilio.entities;

namespace com.mobilio.data.Repositories.Interfaces
{
    public interface IProductRepository
    {
        List<Product> GetProducts();
    }
}
