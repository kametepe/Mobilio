﻿using com.mobilio.entities.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.mobilio.entities
{
    public class Product : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public string SmallImage { get; set; }

        public string BigImage { get; set; }

        public double Price { get; set; }
    }
}
