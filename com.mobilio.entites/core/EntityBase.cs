﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.mobilio.entities.core
{
    [Serializable]
    public class EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public int State { get; set; }
        public DateTime CreationDate { get; set; }
        public Guid CreatorID { get; set; }
        public DateTime EditionDate { get; set; }
        public Guid EditorID { get; set; }
    }
}
