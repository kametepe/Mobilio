﻿using com.mobilio.entities.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.mobilio.entities
{
    public class Testimony : EntityBase
    {
        public string Content { get; set; }
        public string Author { get; set; }
        public string AuthorPosition { get; set; }
        public string authorCompany { get; set; }
    }
}
