﻿using com.mobilio.entities.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.mobilio.entities
{
    public class BlogPost : EntityBase
    {
        public string Title { get; set; }

        public string Content { get; set; }
        public string Author { get; set; }
        public string SmallImage { get; set; }

        public string BigImage { get; set; }
    }
}
