﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace com.mobilio.utilities.Security
{
    public static class PasswordHelper
    {
        public static string HashPassword(string salt, string password)
        {
            string data = string.Concat(password, salt);
            var shaProvider = HashAlgorithm.Create("SHA512");
            var binHash = shaProvider.ComputeHash(Encoding.ASCII.GetBytes(data));
            return (BitConverter.ToString(binHash)).Replace("-", "");
        }

        public static bool IsPasswordStrong(string password)
        {
            //return Regex.Match(password, @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}", RegexOptions.ECMAScript).Success;
            bool status = true;

            if (string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password))
            {
                status = false;
            }

            if (password.Contains(' '))
            {
                status = false;
            }

            if (password.Length < 8 || password.Length > 12)
            {
                status = false;
            }

            if (!Regex.Match(password, @"[!,@@,#,$,%,^,&,*,?,_,~]", RegexOptions.ECMAScript).Success)
            {
                status = false;
            }
            if (!Regex.Match(password, @"[0-9]", RegexOptions.ECMAScript).Success)
            {
                status = false;
            }
            return status;
        }
    }
}
