﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace com.mobilio.utilities.Security
{
    public class Cryptography
    {

        public static string ComputeSha256Hash(string rawData, string salt)
        {
            // Create a SHA256   
            string data = string.Concat(rawData, salt);
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static string HashContent(string salt, string password)
        {
            string data = string.Concat(password, salt);
            var shaProvider = HashAlgorithm.Create("SHA1");
            var binHash = shaProvider.ComputeHash(Encoding.ASCII.GetBytes(data));
            return (BitConverter.ToString(binHash)).Replace("-", "");
        }
    }
}
