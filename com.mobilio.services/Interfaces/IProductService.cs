﻿using com.mobilio.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.mobilio.services.Interfaces
{
    public interface IProductService
    {
        List<Product> GetProducts();
    }
}
