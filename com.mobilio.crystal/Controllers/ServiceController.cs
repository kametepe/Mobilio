using com.mobilio.crystal.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace com.mobilio.crystal.Controllers
{
    public class ServiceController : Controller
    {
        private readonly ILogger<ServiceController> _logger;

        public ServiceController(ILogger<ServiceController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

       
    }
}
