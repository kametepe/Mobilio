using com.mobilio.crystal.Models;
using com.mobilio.services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using com.mobilio.common.Settings;
using Microsoft.Extensions.Options;

namespace com.mobilio.crystal.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IProductService _productService;
        public readonly AppSetting _appSettings;
        public HomeController(ILogger<HomeController> logger, IProductService productService, IOptions<AppSetting> settings)
        {
            _logger = logger;
            _productService = productService;
            _appSettings = settings.Value;
        }

        public IActionResult Index()
        {
            var products = _productService.GetProducts();
            return View(products);
        }

        public IActionResult About()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
