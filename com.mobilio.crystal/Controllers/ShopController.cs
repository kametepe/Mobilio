using com.mobilio.crystal.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace com.mobilio.crystal.Controllers
{
    public class ShopController : Controller
    {
        private readonly ILogger<ShopController> _logger;

        public ShopController(ILogger<ShopController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

    }
}
