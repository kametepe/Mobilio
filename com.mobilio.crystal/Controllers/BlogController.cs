using com.mobilio.crystal.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace com.mobilio.crystal.Controllers
{
    public class BlogController : Controller
    {
        private readonly ILogger<BlogController> _logger;

        public BlogController(ILogger<BlogController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

       
    }
}
