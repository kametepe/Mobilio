using com.mobilio.common.Settings;
using com.mobilio.common;
using com.mobilio.data.Repositories.Interfaces;
using com.mobilio.services.Interfaces;
using com.mobilio.data.Infrastructure;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using NetCore.AutoRegisterDi;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);


builder.Services.Configure<AppSetting>(builder.Configuration.GetSection("AppSetting"));

// Add services to the container.
builder.Services.AddControllersWithViews();

string connection = builder.Configuration.GetConnectionString(Constants.MobilioDBConnection);
builder.Services.AddDbContext<MobilioDBContext>(options => options.UseNpgsql(connection));



builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        options.ExpireTimeSpan = TimeSpan.FromMinutes(20);
        options.SlidingExpiration = true;
        options.AccessDeniedPath = "/Auth/Forbidden/";
        options.LoginPath = "/Auth/Login";

    });


var assemblyToScan = Assembly.GetAssembly(typeof(IProductService));
builder.Services.RegisterAssemblyPublicNonGenericClasses(assemblyToScan)
  .Where(c => c.Name.EndsWith("Service"))
  .AsPublicImplementedInterfaces();

var repoAssemblyToScan = Assembly.GetAssembly(typeof(IProductRepository));
builder.Services.RegisterAssemblyPublicNonGenericClasses(repoAssemblyToScan)
.Where(c => c.Name.EndsWith("Repository"))
.AsPublicImplementedInterfaces();
 

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;

    var context = services.GetRequiredService<MobilioDBContext>();
    context.Database.Migrate();
    context.EnsureSeedData();
}

app.Run();
